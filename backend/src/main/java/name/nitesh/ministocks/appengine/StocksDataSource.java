package name.nitesh.ministocks.appengine;

import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import name.nitesh.ministocks.appengine.YahooStocksData.StockField;
import name.nitesh.ministocks.appengine.utils.PMF;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;

import javax.jdo.PersistenceManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StocksDataSource {

    public static String getStockDataForDisplay() {

        // Cache data for an hour in the memcache
        Map<String, Integer> props = new HashMap<String, Integer>();
        props.put(GCacheFactory.EXPIRATION_DELTA, 3600);

        // Get the cache object
        Cache cache = null;
        try {
            CacheFactory cacheFactory =
                    CacheManager.getInstance().getCacheFactory();
            cache = cacheFactory.createCache(props);
        } catch (CacheException ignored) {
        }

        // If the item exists in the cache return it
        if (cache != null && cache.containsKey("stockData")) {
            return (String) cache.get("stockData");
        }

        // Get yesterdays data and load into a local HashMap
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        String yesterday = formatter.format(calendar.getTime()).toUpperCase();

        PersistenceManager pm = PMF.get().getPersistenceManager();
        String query =
                "select from "
                        + StocksDataStore.class.getName()
                        + " where date == '"
                        + yesterday
                        + "'";

        @SuppressWarnings("unchecked")
        List<StocksDataStore> stocks =
                (List<StocksDataStore>) pm.newQuery(query).execute();
        // pm.deletePersistentAll(stocks);

        // Display friendly version
        JSONObject data = new JSONObject();
        if (!stocks.isEmpty()) {

            for (StocksDataStore stock : stocks) {

                JSONObject item = new JSONObject();
                try {
                    item.put("symbol", stock.getSymbol());
                    item.put("price", stock.getPrice());
                    data.put(stock.getSymbol(), item);

                } catch (Exception ignored) {
                }
            }
        }
        pm.close();

        // Put the data in the memcache
        if (cache != null) {
            cache.put("stockData", data.toString());
        }

        return data.toString();
    }

    public static final String[] CURRENCIES = {
            "AED",
            "AUD",
            "CAD",
            "CHF",
            "CNY",
            "EUR",
            "GBP",
            "HUF",
            "INR",
            "JPY",
            "KRW",
            "MYR",
            "NZD",
            "THB",
            "USD",
            "XAU",
            "TRY",
            "ZAR"};

    public static HashMap<String, HashMap<StockField, String>> getStockData() {
        return YahooStocksData.getStocksData(getSymbols());
    }

    public static void updateCurrencyData() {

        PersistenceManager pm = PMF.get().getPersistenceManager();

        // Add data to the database
        HashMap<String, HashMap<StockField, String>> stockData = getStockData();
        for (String symbol : stockData.keySet()) {

            // Create new record
            HashMap<StockField, String> stock = stockData.get(symbol);
            StocksDataStore rec =
                    new StocksDataStore(
                            symbol,
                            stock.get(StockField.NAME),
                            stock.get(StockField.PRICE),
                            stock.get(StockField.CHANGE),
                            stock.get(StockField.PERCENT),
                            stock.get(StockField.EXCHANGE));

            pm.makePersistent(rec);
        }
        pm.close();
    }

    public static String[] getSymbols() {

        // Create all currency combinations as a space-separated string
        StringBuilder fx = new StringBuilder();
        for (String c1 : CURRENCIES)
            for (String c2 : CURRENCIES)
                if (!c1.equals(c2)) {
                    fx.append(" ").append(c1).append(c2).append("=X");

                    // Special case for USD rates for Yahoo
                    if (c1.equals("USD")) {
                        fx.append(" ").append(c2).append("=X");
                    }
                }

        // Remove leading whitespace and return as an array of strings
        return fx.substring(1).split(" ");
    }
}
