package name.nitesh.ministocks.appengine.servlets;

import name.nitesh.ministocks.appengine.StocksDataSource;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings("serial")
public class GetCurrencyDataServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {

        resp.setContentType("text/plain");
        resp.getWriter().println(StocksDataSource.getStockDataForDisplay());
    }
}
