package name.nitesh.ministocks.appengine;

import java.util.Arrays;
import java.util.HashMap;

import name.nitesh.ministocks.appengine.utils.URLData;

public class YahooStocksData {

	public enum StockField {
		PRICE, CHANGE, PERCENT, EXCHANGE, NAME
	}

	private static final String BASE_URL =
			"http://download.finance.yahoo.com/d/quotes.csv";

	// s: symbol
	// l1: price
	// c1: change
	// p2: percentage change
	// x: exchange
	// n: name

	private static final String FORMAT = "sl1c1p2xn";

	public static HashMap<String, HashMap<StockField, String>> getStocksData(
			String[] symbols) {

		// Create empty HashMap to store the results
		HashMap<String, HashMap<StockField, String>> quotes =
				new HashMap<String, HashMap<StockField, String>>();

		// Get the stocks in groups of 180 since Yahoo supports a max of 200
		// stocks per request
		int end = symbols.length;
		int start = end - 180;

		while (end > 0) {
			if (start < 0) {
				start = 0;
			}

			// Copy the group symbols from the full list
			String[] symbols_group = Arrays.copyOfRange(symbols, start, end);

			// Move counters for the next group
			end -= 180;
			start = end - 180;

			// Convert the list of stock symbols into a url-encoded parameter
			StringBuilder s_query = new StringBuilder();
			for (String s : symbols_group) {

				// If the query string is not empty add a '+'
				if (!s_query.toString().equals(""))
					s_query.append("+");

				s_query.append(s);
			}

			// Build the data source url and get the data
			String url = BASE_URL + "?f=" + FORMAT + "&s=" + s_query.toString();
			String response = URLData.getURLData(url);

			// Add the symbols to the hashmap
			for (String s : symbols_group)
				quotes.put(s, new HashMap<StockField, String>());

			// Parse the response and return
			for (String line : response.split("\n")) {

				// Replace quotes and split values into an array
				String[] values =
						line.replace("\"", "").split(
								",",
								StockField.values().length + 1);

				// If we do not have at least 6 elements continue
				if (values.length < StockField.values().length + 1) {
					continue;
				}

				quotes.get(values[0]).put(StockField.PRICE, values[1]);
				quotes.get(values[0]).put(StockField.CHANGE, values[2]);
				quotes.get(values[0]).put(StockField.PERCENT, values[3]);
				quotes.get(values[0]).put(StockField.EXCHANGE, values[4]);
				quotes.get(values[0]).put(StockField.NAME, values[5]);
			}
		}

		// Return quotes
		return quotes;
	}
}
