package name.nitesh.ministocks.appengine.servlets;

import name.nitesh.ministocks.appengine.StocksDataSource;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings("serial")
public class UpdateCurrencyDataServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {

        // Update database with todays data
        StocksDataSource.updateCurrencyData();

        resp.setContentType("text/plain");
        resp.getWriter().println("200 OK\n\nUpdateCurrencyDataServlet\n\n");
    }
}
