package name.nitesh.ministocks.appengine.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;

import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

public class URLData {

	/* URL data retrieval without caching */
	public static String getURLData2(String url) {

		// Grab the data from the source
		StringBuilder urlData = new StringBuilder();
		try {
			BufferedReader reader =
					new BufferedReader(new InputStreamReader(
							new URL(url).openStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				urlData.append(line).append("\n");
			}
			reader.close();

		} catch (MalformedURLException ignored) {
		} catch (IOException ignored) {
		}

		return urlData.toString();
	}

	/* URL data retrieval helper */
	public static String getURLData(String url) {
		return getURLData(url, false);
	}

	/* URL data retrieval that supports caching */
	public static String getURLData(String url, Boolean useCache) {

		String data;
		Cache cache = getCache();

		// Return cached data if we have it
		if (useCache && cache != null) {
			data = (String) cache.get(url);
			if (data != null) {
				return data;
			}
		}

		// Get fresh data from the URL
		data = getURLData2(url);

		// Update cache if we have one
		if (cache != null) {
			cache.put(url, data);
		}
		return data;
	}

	/* Cache instance that caches items for 15 minutes */
	public static Cache getCache() {

		// Set 15 minute cache (900 seconds)
		HashMap<Object, Object> props = new HashMap<Object, Object>();
		props.put(GCacheFactory.EXPIRATION_DELTA, 900);

		Cache cache = null;
		try {
			CacheFactory cacheFactory =
					CacheManager.getInstance().getCacheFactory();
			cache = cacheFactory.createCache(props);

		} catch (CacheException ignored) {
		}

		return cache;
	}
}
