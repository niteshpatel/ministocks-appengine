package name.nitesh.ministocks.appengine;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class StocksDataStore {

    public StocksDataStore(
            String symbol,
            String name,
            String price,
            String change,
            String percent,
            String exchange) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        String today = formatter.format(new Date()).toUpperCase();
        this.date = today;

        this.key = symbol + "|" + today;
        this.symbol = symbol;
        this.name = name;
        this.price = price;
        this.change = change;
        this.percent = percent;
        this.exchange = exchange;
    }

    @PrimaryKey
    private String key;

    public String getKey() {
        return key;
    }

    @Persistent
    private String date;

    public String getDate() {
        return date;
    }

    @Persistent
    private String symbol;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Persistent
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Persistent
    private String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Persistent
    private String change;

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    @Persistent
    private String percent;

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    @Persistent
    private String exchange;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }
}